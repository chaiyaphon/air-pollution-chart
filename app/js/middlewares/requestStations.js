import { put, call, takeLatest } from "redux-saga/effects"
import { getStationSuccess, getStationFail } from "../actions"
import * as acitionTypes from "../actions/types"
import axios from "axios"

export default function* watcherRequestStations() {
  yield takeLatest(acitionTypes.GET_STATIONS, workerRequestStations)
}

function fetchStations() {
  return axios.get("http://air4thai.pcd.go.th/services/getAQI_JSON.php")
}

function* workerRequestStations() {
  try {
    const getStationsResponse = yield call(fetchStations)
    const stationsResponseData = getStationsResponse.data.stations
    yield put(getStationSuccess(stationsResponseData))
  } catch (error) {
    yield put(getStationFail(error))
  }
}
