import { applyMiddleware, createStore } from "redux"
import createSagaMiddleWare from "redux-saga"
import rootReducer from "../reducers"
import watcherRequestStations from "../middlewares/requestStations"

const sagaMidleware = createSagaMiddleWare()

const store = createStore(rootReducer, applyMiddleware(sagaMidleware))

sagaMidleware.run(watcherRequestStations)

export default store
