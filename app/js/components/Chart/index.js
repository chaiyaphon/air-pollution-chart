import React, { Component } from "react"
import PropTypes from "prop-types"

class Chart extends Component {
  static propTypes = {
    hCentering: PropTypes.bool,
    diffChart: PropTypes.bool,
    chartType: PropTypes.oneOf([
      "AreaChart",
      "Bar",
      "BubbleChart",
      "ColumnChart",
      "ComboChart",
      "GeoChart",
      "LineChart",
      "PieChart"
    ]),
    chartTitle: PropTypes.string,
    chartSubTitle: PropTypes.string,
    chartData: PropTypes.array,
    chartOptions: PropTypes.object,
    chartWidth: PropTypes.number,
    chartHeight: PropTypes.number
  }

  static defaultProps = {
    chartData: [],
    chartOptions: {}
  }

  componentDidMount = async () => {
    google.charts.load("current", {
      packages: ["corechart", "bar"]
    })
    google.charts.setOnLoadCallback(this.drawChart)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.chartData !== this.props.chartData) {
      google.charts.setOnLoadCallback(this.drawChart)
    }
  }

  generateChartData = chart => {
    if (this.props.diffChart) {
      const dataTableList = this.props.chartData.map(data =>
        google.visualization.arrayToDataTable(data)
      )
      return chart.computeDiff(...dataTableList)
    }
    return google.visualization.arrayToDataTable(this.props.chartData)
  }

  drawChart = () => {
    if (this.props.chartData.length > 0) {
      const { chartType } = this.props
      const chartConstructor =
        chartType === "Bar" ? google.charts : google.visualization

      const chart = new chartConstructor[chartType](this.chartNode)
      chart.draw(this.generateChartData(chart), this.props.chartOptions)
    }
  }

  render() {
    return <div className="chart-div" ref={elem => (this.chartNode = elem)} />
  }
}

export default Chart
