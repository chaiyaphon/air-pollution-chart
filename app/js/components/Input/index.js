import React from "react"
import PropTypes from "prop-types"
import "./index.scss"

const Input = props => {
  const {
    type,
    handlerOnChange,
    style,
    placeholder,
    name,
    value,
    checked
  } = props

  return (
    <input
      type={type}
      className="input-block"
      onChange={handlerOnChange}
      style={style}
      placeholder={placeholder}
      name={name}
      value={value}
      checked={checked}
    />
  )
}

Input.propTypes = {
  type: PropTypes.string,
  handlerOnChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool
}

Input.defaultProps = {
  type: "text"
}

export default Input
