import React from 'react'
import PropTypes from 'prop-types'

function generateOptionElem(listData, placeholder) {
  try {
    const optionElem = listData.map(data => (
      <option key={data} value={data}>
        {data}
      </option>
    ))

    return [
      <option key="default" value="">
        {placeholder}
      </option>,
      ...optionElem
    ]
  } catch (err) {
    return (
      <option key="default" value="">
        {placeholder}
      </option>
    )
  }
}

const SelectOption = props => {
  const {
    className,
    name,
    value,
    handlerOnSelect,
    require,
    placeholder,
    listDataForGenerateOptionElem
  } = props

  return (
    <select
      className={className}
      name={name}
      value={value}
      onChange={handlerOnSelect}
      required={require}
    >
      {generateOptionElem(listDataForGenerateOptionElem, placeholder)}
    </select>
  )
}

SelectOption.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  require: PropTypes.bool,
  handlerOnSelect: PropTypes.func,
  placeholder: PropTypes.string,
  listDataForGenerateOptionElem: PropTypes.array
}

export default SelectOption
