import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import "./index.scss"

const Card = props => {
  const { children, className, header, minHeight, verticalAlignAtBody } = props
  return (
    <div className={classNames("card", className)} style={{ minHeight }}>
      {header ? <div className="card-header">{header}</div> : false}
      <div
        className={classNames(
          "card-body",
          verticalAlignAtBody ? "grid-centering" : ""
        )}
      >
        {children}
      </div>
    </div>
  )
}

Card.defaultProps = {
  children: ""
}

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ]),
  className: PropTypes.string,
  header: PropTypes.string,
  minHeight: PropTypes.string,
  verticalAlignAtBody: PropTypes.bool
}

export default Card
