import * as actionTypes from "./types"

export const getStations = () => ({
  type: actionTypes.GET_STATIONS
})

export const getStationSuccess = stations => ({
  type: actionTypes.GET_STATIONS_SUCCESS,
  stations: stations,
  dataLength: stations.length
})

export const getStationFail = error => ({
  type: actionTypes.GET_STATIONS_FAIL,
  error
})
