import React, { Component } from 'React'
import { connect } from 'react-redux'
import { Card, Chart } from '../../components'
import {
  getAirPollutionDataFromStationList,
  getAirPollutionKeyName
} from '../../logic/airPollutionDataOps'

class PollutionSlideShow extends Component {
  state = {
    chartDataTableIndex: 0
  }

  componentDidMount() {
    this.drawChartInterval = setInterval(() => {
      this.chartIndexRunner()
    }, 5000)
  }

  chartIndexRunner = () => {
    this.setState(
      (prevStat, prevProps) =>
        prevStat.chartDataTableIndex < prevProps.stationsLength - 1
          ? { chartDataTableIndex: prevStat.chartDataTableIndex + 1 }
          : { chartDataTableIndex: 0 }
    )
  }

  getChartDataTableByIndex = dataTableIndex => {
    const { stations } = this.props,
      pollutionDataKey = 'LastUpdate',
      stationsNameKey = 'nameEN',
      discardPollutionDataKey = ['date', 'time']

    const airPollutionDataTableHeader = [
      'Station Name EN',
      ...getAirPollutionKeyName(
        stations[dataTableIndex],
        pollutionDataKey,
        discardPollutionDataKey
      )
    ]

    const airPolluionDataList = getAirPollutionDataFromStationList(
      stations,
      stationsNameKey,
      pollutionDataKey,
      discardPollutionDataKey
    )

    return [
      airPollutionDataTableHeader,
      [...airPolluionDataList[dataTableIndex]]
    ]
  }

  componentWillUnmount() {
    clearInterval(this.drawChartInterval)
  }

  render() {
    const { stations } = this.props,
      { chartDataTableIndex } = this.state

    return (
      <div className="content-block">
        <h2>Pollution in each station</h2>
        <Card
          className="card-chart"
          header={`Air pollution in ${
            stations[chartDataTableIndex].nameEN
          } at ${stations[chartDataTableIndex].LastUpdate.time} on ${
            stations[chartDataTableIndex].LastUpdate.date
          }.`}
          minHeight="300px"
          verticalAlignAtBody
        >
          <Chart
            chartType="Bar"
            chartData={this.getChartDataTableByIndex(
              this.state.chartDataTableIndex
            )}
            chartOptions={{
              legend: { position: 'top', maxLines: 3 },
              bar: { groupWidth: '25%' }
            }}
          />
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  stations: state.AirPollutionsReducer.stations,
  stationsLength: state.AirPollutionsReducer.stationsLength
})

export default connect(mapStateToProps)(PollutionSlideShow)
