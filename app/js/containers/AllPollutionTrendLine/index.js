import React, { Component } from "react"
import { connect } from "react-redux"
import { Card, Chart } from "../../components"
import {
  getAirPollutionKeyName,
  getSumValueOfPollution,
  getHighesValueOfPollution
} from "../../logic/airPollutionDataOps"

class AllPollutionTrendLine extends Component {
  generateDataTable = () => {
    const { stations, stationsLength } = this.props

    const dataRow = getAirPollutionKeyName(stations[0]).map(keyName => {
      return [
        keyName,
        getHighesValueOfPollution(this.props.stations, keyName),
        getSumValueOfPollution(this.props.stations, keyName) / stationsLength
      ]
    })

    return [["Pollution", "Value", "Average"], ...dataRow]
  }

  render() {
    return (
      <div className="content-block">
        <h2>Highest and Average value of pollution in Thailand</h2>
        <Card className="card-chart">
          <Chart
            chartType="ComboChart"
            chartData={this.generateDataTable()}
            chartOptions={{
              height: 450,
              vAxis: { title: "Value" },
              hAxis: { title: "Pollutions" },
              seriesType: "bars",
              series: { 1: { type: "line" } }
            }}
          />
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  stations: state.AirPollutionsReducer.stations,
  stationsLength: state.AirPollutionsReducer.stationsLength
})

export default connect(mapStateToProps)(AllPollutionTrendLine)
