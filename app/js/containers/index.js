import React, { Component, Fragment } from "react"
import ChartOverview from "./ChartOverview"
import AllPollutionTrendLine from "./AllPollutionTrendLine"
import PollutionSlideShow from "./PollutionSlideShow"
import PollutionComparison from "./PollutionComparison"
import { getStations } from "../actions"
import { connect } from "react-redux"

class Root extends Component {
  componentDidMount() {
    this.props.dispatch(getStations())
  }

  render() {
    const { error, fetching, stationsLength } = this.props

    const renderElem = error ? (
      <h1>Error cause during fetch data</h1>
    ) : fetching || stationsLength === 0 ? (
      <p>Loading...</p>
    ) : (
      <Fragment>
        <div className="flex">
          <div className="left-chart">
            <ChartOverview />
          </div>
          <div className="right-chart">
            <AllPollutionTrendLine />
          </div>
        </div>
        <div className="flex">
          <div className="left-chart">
            <PollutionSlideShow />
          </div>
          <div className="right-chart">
            <PollutionComparison />
          </div>
        </div>
      </Fragment>
    )

    return <div className="app-container">{renderElem}</div>
  }
}

const mapStateToPropts = state => ({
  fetching: state.AirPollutionsReducer.fetching,
  error: state.AirPollutionsReducer.error,
  stationsLength: state.AirPollutionsReducer.stationsLength
})

export default connect(mapStateToPropts)(Root)
