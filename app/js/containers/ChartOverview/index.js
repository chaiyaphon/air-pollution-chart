import React, { Component } from "react"
import { connect } from "react-redux"
import { Card, Chart, SelectOption } from "../../components"
import {
  getGeoDataListByLatitude,
  getAirPollutionKeyName
} from "../../logic/airPollutionDataOps"

class ChartOverview extends Component {
  state = {
    pollutionName: "PM10"
  }

  initSelectOption = () => {
    const { stations } = this.props

    return getAirPollutionKeyName(stations[0])
  }

  _onSelectPollution = evt => {
    const { name, value } = evt.target
    if (value !== "") {
      this.setState({ [name]: value })
    }
  }

  getDataTableFromStations = () => {
    const geoData = getGeoDataListByLatitude(
      this.props.stations,
      "nameEN",
      this.state.pollutionName
    )

    const headerData = ["Staton Name", "Latitude", "Pollution Value"]

    return [headerData, ...geoData]
  }

  render() {
    return (
      <div className="content-block">
        <h2>Air Pollution Overview</h2>
        <Card className="card-chart">
          <div className="flex-between">
            <h3 className="mt-0">
              {this.state.pollutionName} pollution in each statation
            </h3>
            <SelectOption
              name="pollutionName"
              className="mb-1"
              placeholder="Select the pollution here"
              handlerOnSelect={this._onSelectPollution}
              listDataForGenerateOptionElem={this.initSelectOption()}
            />
          </div>

          <Chart
            chartType="GeoChart"
            chartData={this.getDataTableFromStations()}
            chartOptions={{
              height: 400,
              region: "TH",
              displayMode: "markers",
              colorAxis: { colors: ["#FFCB91", "#E42815"] },
              backgroundColor: "#1B6BC8",
              datalessRegionColor: "#93C592"
              // defaultColor: '#f5f5f5'
            }}
          />
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  stations: state.AirPollutionsReducer.stations,
  stationsLength: state.AirPollutionsReducer.stationsLength
})

export default connect(mapStateToProps)(ChartOverview)
