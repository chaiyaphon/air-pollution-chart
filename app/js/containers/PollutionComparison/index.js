import React, { Component } from "react"
import { connect } from "react-redux"
import { Chart, Card, SelectOption, Input } from "../../components"
import {
  getStationNameList,
  getAirPollutionDataFromStation,
  getAirPollutionKeyName
} from "../../logic/airPollutionDataOps"

let optionList = []

class PollutionComparison extends Component {
  state = {
    stationOptions1: [],
    stationOptions2: [],
    station1: "",
    station2: "",
    chartDataTable: []
  }

  componentDidMount() {
    optionList = this.generateOptionDataFromStations("nameEN")

    this.setState({
      stationOptions1: optionList,
      stationOptions2: optionList
    })
  }

  generateOptionDataFromStations = targetKey => {
    return getStationNameList(this.props.stations, targetKey)
  }

  _onSelectStation = evt => {
    const { name, value } = evt.target
    const stationListName = this.mappingSelectedStationToStationList(name)

    this.setState({
      [name]: value,
      [stationListName]: this.removeObjectFromOptionList(optionList, value)
    })
  }

  mappingSelectedStationToStationList(selectedStation) {
    switch (selectedStation) {
    case "station1":
      return "stationOptions2"
    case "station2":
      return "stationOptions1"
    }
  }

  removeObjectFromOptionList(optionListForRemove, optionNameForRemove) {
    const newOptionList = optionListForRemove.filter(
      option => option !== optionNameForRemove
    )

    return newOptionList
  }

  generateChartDataTable = () => {
    const { station1, station2 } = this.state,
      discardValue = ["date", "time"],
      pollutionDataKeyName = "LastUpdate"

    const headerRow = [
      "Station Name EN",
      ...getAirPollutionKeyName(
        this.props.stations[0],
        pollutionDataKeyName,
        discardValue
      )
    ]

    const dataRowList = this.reIndexStationListBySelectedStation(
      this.props.stations
        .filter(
          station => station.nameEN === station1 || station.nameEN === station2
        )
        .map(station =>
          getAirPollutionDataFromStation(
            station,
            "nameEN",
            "LastUpdate",
            discardValue
          )
        )
    )

    return dataRowList.map(dataRow =>
      dataRow.map((data, index) => [headerRow[index], data])
    )
  }

  reIndexStationListBySelectedStation = dataList => {
    const { station2 } = this.state

    if (dataList[0][0] === station2) {
      return [dataList[1], dataList[0]]
    }
    return dataList
  }

  _onClickCompareBtn = evt => {
    evt.preventDefault()
    if (this.selectStationFormValidator()) {
      alert("Please, select station for compare the pollution")
    } else {
      const chartDataTable = this.generateChartDataTable()

      this.setState({
        chartDataTable: chartDataTable
      })
    }
  }

  selectStationFormValidator = () => {
    return this.state.station1 === "" || this.state.station2 === ""
  }

  render() {
    return (
      <div className="content-block">
        <h2>Pollution Comparison</h2>
        <Card className="card-chart" minHeight="300px">
          <div className="flex-between mb-1">
            <form id="compare-pollution-form">
              <div className="width-min-content">
                <div className="flex">
                  <label className="mr-1">Previous</label>
                  <SelectOption
                    className="mb-1"
                    name="station1"
                    placeholder="Select first station"
                    handlerOnSelect={this._onSelectStation}
                    require
                    listDataForGenerateOptionElem={this.state.stationOptions1}
                  />
                </div>
                <div className="flex">
                  <label className="mr-1">Current</label>
                  <SelectOption
                    name="station2"
                    placeholder="Select second station for compare"
                    handlerOnSelect={this._onSelectStation}
                    require
                    listDataForGenerateOptionElem={this.state.stationOptions2}
                  />
                </div>
              </div>
            </form>
            <button
              type="submit"
              form="compare-pollution-form"
              onClick={this._onClickCompareBtn}
            >
              Compare
            </button>
          </div>
          <Chart
            diffChart
            chartType="PieChart"
            chartData={this.state.chartDataTable}
            chartOptions={{
              pieSliceText: "none",
              vAxis: { title: "Value" },
              hAxis: { title: "Station" },
              seriesType: "bars",
              series: { 5: { type: "line" } }
            }}
          />
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  stations: state.AirPollutionsReducer.stations
})

export default connect(mapStateToProps)(PollutionComparison)
