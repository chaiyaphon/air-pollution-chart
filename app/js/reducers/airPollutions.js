import * as actionTypes from "../actions/types"

const initialState = {
  stations: [],
  stationsLength: 0,
  fetching: true,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
  case actionTypes.GET_STATIONS:
    return {
      ...state,
      fetching: true,
      stations: [],
      stationsLength: 0
    }
  case actionTypes.GET_STATIONS_SUCCESS:
    return {
      ...state,
      stations: action.stations,
      stationsLength: action.dataLength,
      fetching: false
    }
  case actionTypes.GET_STATIONS_FAIL:
    return {
      fetching: false,
      stations: [],
      stationsLength: 0,
      error: action.error
    }
  default:
    return state
  }
}
