import { combineReducers } from "redux"
import AirPollutions from "./airPollutions"

const RootReducer = combineReducers({
  AirPollutionsReducer: AirPollutions
})

export default RootReducer
