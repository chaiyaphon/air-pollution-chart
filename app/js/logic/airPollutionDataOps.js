const _ = require("lodash")

export const getAirPollutionKeyName = objectData => {
  const discardPollutionDataKey = ["date", "time"]

  return Object.keys(_.omit(objectData.LastUpdate, discardPollutionDataKey))
}

function parseIntValue(value) {
  const intValue = parseInt(value)
  return isNaN(intValue) ? 0 : intValue
}

export const getAirPollutionDataFromStationList = (
  stations,
  displayName,
  targetToDiscardItem,
  discardItemList
) => {
  try {
    return stations.map(station =>
      getAirPollutionDataFromStation(
        station,
        displayName,
        targetToDiscardItem,
        discardItemList
      )
    )
  } catch (err) {
    return []
  }
}

export const getAirPollutionDataFromStation = (
  station,
  displayName,
  targetToDiscardItem,
  discardItemList
) => {
  return [
    station[displayName],
    ...getAirPollutionKeyName(
      station,
      targetToDiscardItem,
      discardItemList
    ).map(item =>
      parseIntValue(
        station.LastUpdate[item].value || station.LastUpdate[item].Level
      )
    )
  ]
}

export const getStationNameList = (objectList, targetKey) => {
  try {
    return [...objectList.map(object => object[targetKey])]
  } catch (err) {
    return []
  }
}

export const getPollutionValueByPollutionKey = pollutionObj => {
  return pollutionObj.value || pollutionObj.Level
}

export const getGeoDataListByLatitude = (
  stations,
  displayName,
  pollutionKey
) => {
  try {
    return stations.map(station => [
      station[displayName],
      parseFloat(station.lat),
      parseIntValue(
        getPollutionValueByPollutionKey(station.LastUpdate[pollutionKey])
      )
    ])
  } catch (error) {
    return []
  }
}

export const getSumValueOfPollution = (stationList, pollutionName) => {
  try {
    let averagePollutionValue = 0
    stationList.forEach(station => {
      averagePollutionValue += parseIntValue(
        getPollutionValueByPollutionKey(station.LastUpdate[pollutionName])
      )
    })
    return averagePollutionValue
  } catch (error) {
    return 0
  }
}

export const getHighesValueOfPollution = (stationList, pollutionName) => {
  try {
    let highestPollutionValue = 0,
      tmp = 0
    stationList.forEach(station => {
      tmp = parseIntValue(
        getPollutionValueByPollutionKey(station.LastUpdate[pollutionName])
      )

      if (tmp > highestPollutionValue) highestPollutionValue = tmp
    })
    return highestPollutionValue
  } catch (error) {
    return 0
  }
}
