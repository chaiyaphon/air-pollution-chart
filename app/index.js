import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import configureStore from "./js/stores/configureStore"

import Root from "./js/containers"
import "./scss/global.scss"

ReactDOM.render(
  <Provider store={configureStore}>
    <Root />
  </Provider>,
  document.getElementById("app")
)

module.hot.accept()
